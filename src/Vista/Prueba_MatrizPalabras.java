/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.MatrizPalabra;
import Negocio.Tiempo_Ejecucion;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class Prueba_MatrizPalabras {
    
    public static void main(String[] args) {
        try {
            ArchivoLeerURL archivo=new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt");
            Scanner entrada = new Scanner(System.in);
            Object lineas[]=archivo.leerArchivo();
            System.out.println("INGRESE EL NUMERO DE PALABRAS A ORDENAR: ");
            int canLineas=entrada.nextInt();
            MatrizPalabra m=new MatrizPalabra(lineas,canLineas);
            

//               Experimento_normal( m, canLineas);
               Experimento_Mejorado( m, canLineas);
            
            
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
         
    }
    
    public static void Experimento_normal(MatrizPalabra m, int canLineas){
       
        Tiempo_Ejecucion tiempo = new Tiempo_Ejecucion(); 
        m.OrdenarBurbuja();
        tiempo.stop();
        System.out.println("se demoró en ordenar: " +tiempo.getTiempoMillis() +" Milisegundos ");
        
        
    
    }
    
    public static void Experimento_Mejorado(MatrizPalabra m, int canLineas){
       
        Tiempo_Ejecucion tiempo = new Tiempo_Ejecucion(); 
        m.OrdenarBurbujaMejorado();
        tiempo.stop();
        System.out.println("se demoró en ordenar: " +tiempo.getTiempoMillis() +" milisegundos ");
        
        
    
    }
    
}
